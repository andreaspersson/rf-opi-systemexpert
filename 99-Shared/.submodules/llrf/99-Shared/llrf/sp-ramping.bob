<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(SYSNAME=LLRF) - SP Ramping</name>
  <width>920</width>
  <height>960</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1200</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>$(SYSNAME=LLRF) - SP Ramping</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>410</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="xyplot" version="2.0.0">
    <name>X/Y Plot_5</name>
    <x>380</x>
    <y>60</y>
    <width>530</width>
    <height>340</height>
    <title>SP Table</title>
    <x_axis>
      <title>ms</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>100.0</maximum>
      <show_grid>false</show_grid>
      <title_font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Cavity Field [kV]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </y_axis>
      <y_axis>
        <title>Phase</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>-3.2</minimum>
        <maximum>3.2</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>MAG</name>
        <x_pv>$(PD=LLRF:)$(RD=DIG01:)CtrlTbl-XAxis</x_pv>
        <y_pv>$(PD=LLRF:)$(RD=DIG01:)SPTbl-Mag</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>1</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>ANG</name>
        <x_pv>$(PD=LLRF:)$(RD=DIG01:)CtrlTbl-XAxis</x_pv>
        <y_pv>$(PD=LLRF:)$(RD=DIG01:)SPTbl-Ang</y_pv>
        <err_pv></err_pv>
        <axis>1</axis>
        <trace_type>1</trace_type>
        <color>
          <color name="MAJOR" red="252" green="13" blue="27">
          </color>
        </color>
        <line_width>1</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Pulse generation</name>
    <x>10</x>
    <y>70</y>
    <width>350</width>
    <height>330</height>
    <widget type="label" version="2.0.0">
      <name>Label_9</name>
      <class>CAPTION</class>
      <text>Enable</text>
      <y>19</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="slide_button" version="2.0.0">
      <name>Slide Button_2</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingEn</pv_name>
      <label></label>
      <x>249</x>
      <y>19</y>
      <width>50</width>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_10</name>
      <class>CAPTION</class>
      <text>Cavity Field A:</text>
      <y>79</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_11</name>
      <class>CAPTION</class>
      <text>Filling time:</text>
      <y>119</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_13</name>
      <class>CAPTION</class>
      <text>Tao:</text>
      <y>159</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingA</pv_name>
      <x>159</x>
      <y>79</y>
      <width>140</width>
      <height>30</height>
      <precision>2</precision>
      <show_units>true</show_units>
      <maximum>1000.0</maximum>
      <increment>10.0</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner_2</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingTfill</pv_name>
      <x>159</x>
      <y>119</y>
      <width>140</width>
      <height>30</height>
      <show_units>true</show_units>
      <increment>0.1</increment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner_3</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingTao</pv_name>
      <x>159</x>
      <y>159</y>
      <width>140</width>
      <height>30</height>
      <show_units>true</show_units>
      <increment>0.1</increment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_14</name>
      <class>CAPTION</class>
      <text>Phase:</text>
      <y>239</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner_4</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingPhase</pv_name>
      <x>159</x>
      <y>239</y>
      <width>140</width>
      <height>30</height>
      <show_units>true</show_units>
      <increment>0.1</increment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_15</name>
      <class>CAPTION</class>
      <text>Delay:</text>
      <y>199</y>
      <width>130</width>
      <height>30</height>
      <foreground_color use_class="true">
        <color name="Text" red="25" green="25" blue="25">
        </color>
      </foreground_color>
      <horizontal_alignment use_class="true">2</horizontal_alignment>
      <vertical_alignment use_class="true">1</vertical_alignment>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner_5</name>
      <pv_name>$(P=LLRF:)$(R=)SPRampingDelay</pv_name>
      <x>159</x>
      <y>199</y>
      <width>140</width>
      <height>30</height>
      <show_units>true</show_units>
      <increment>0.1</increment>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_22</name>
    <text>Maximum pulse length:</text>
    <x>370</x>
    <y>410</y>
    <width>200</width>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(P=LLRF:)$(R=)SPRampingT</pv_name>
    <x>570</x>
    <y>410</y>
    <width>140</width>
  </widget>
</display>
