<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>Polynomial fit calibration - Output - $(PI_TYPE=FF)</name>
  <width>1520</width>
  <height>1130</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1520</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>Polynomial fit calibration - Output - $(PI_TYPE=FF)</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>920</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="group" version="2.0.0">
    <name>MGGrey03</name>
    <y>50</y>
    <width>1520</width>
    <height>1080</height>
    <style>3</style>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_1</name>
      <width>1520</width>
      <height>1080</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>X/Y Plot</name>
      <x>40</x>
      <y>40</y>
      <width>470</width>
      <height>340</height>
      <title>Calibration Values - Raw to EGU</title>
      <show_legend>false</show_legend>
      <tooltip></tooltip>
      <x_axis>
        <title>Raw Values</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>EGU Values</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>-10.0</minimum>
          <maximum>10.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Calibration Values</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
        <trace>
          <name>Fitted curve</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalFitLinR2E</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="255" green="0" blue="0">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="slide_button" version="2.0.0">
      <name>Slide Button_1</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEn</pv_name>
      <label></label>
      <x>150</x>
      <y>966</y>
      <width>80</width>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_4</name>
      <text>Enable</text>
      <x>70</x>
      <y>970</y>
      <width>60</width>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalOrdR2E</pv_name>
      <x>850</x>
      <y>450</y>
      <width>70</width>
      <height>30</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_68</name>
      <text>Degree</text>
      <x>760</x>
      <y>450</y>
      <width>60</width>
      <height>30</height>
    </widget>
    <widget type="text-symbol" version="2.0.0">
      <name>Text Symbol_1</name>
      <symbols>
        <symbol>↦</symbol>
      </symbols>
      <x>540</x>
      <y>130</y>
      <width>100</width>
      <height>90</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="72.0">
        </font>
      </font>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array_1</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalCoefR2E</pv_name>
      <x>710</x>
      <y>70</y>
      <width>240</width>
      <height>350</height>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_1</name>
        <width>220</width>
        <height>30</height>
        <format>3</format>
        <precision>3</precision>
      </widget>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label</name>
      <class>HEADER2</class>
      <text>Calculated Coefficients </text>
      <x>720</x>
      <y>20</y>
      <width>220</width>
      <height>50</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_4</name>
      <actions>
        <action type="open_display">
          <file>full-scaling_pi.bob</file>
          <macros>
            <FIT>R2E</FIT>
            <XAX>Raw</XAX>
            <YAX>EGU</YAX>
          </macros>
          <target>window</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>...</text>
      <x>470</x>
      <y>360</y>
      <width>40</width>
      <height>20</height>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="spinner" version="2.0.0">
      <name>Spinner_1</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalOrdE2R</pv_name>
      <x>850</x>
      <y>950</y>
      <width>70</width>
      <height>30</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_69</name>
      <text>Degree</text>
      <x>760</x>
      <y>950</y>
      <width>60</width>
      <height>30</height>
    </widget>
    <widget type="text-symbol" version="2.0.0">
      <name>Text Symbol_2</name>
      <symbols>
        <symbol>↦</symbol>
      </symbols>
      <x>540</x>
      <y>630</y>
      <width>100</width>
      <height>90</height>
      <font>
        <font family="Source Sans Pro" style="REGULAR" size="72.0">
        </font>
      </font>
    </widget>
    <widget type="array" version="2.0.0">
      <name>Array_2</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalCoefE2R</pv_name>
      <x>710</x>
      <y>570</y>
      <width>240</width>
      <height>350</height>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_2</name>
        <width>220</width>
        <height>30</height>
        <format>3</format>
        <precision>3</precision>
      </widget>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_70</name>
      <class>HEADER2</class>
      <text>Calculated Coefficients </text>
      <x>720</x>
      <y>520</y>
      <width>220</width>
      <height>50</height>
      <font use_class="true">
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>X/Y Plot_3</name>
      <x>40</x>
      <y>560</y>
      <width>470</width>
      <height>340</height>
      <title>Calibration Values - EGU to Raw</title>
      <show_legend>false</show_legend>
      <tooltip></tooltip>
      <x_axis>
        <title>EGU Values</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title>Raw Values</title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>-10.0</minimum>
          <maximum>10.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Calibration Values</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
        <trace>
          <name>Fitted curve</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalFitLinE2R</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="255" green="0" blue="0">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>X/Y Plot_4</name>
      <x>1020</x>
      <y>70</y>
      <width>470</width>
      <height>340</height>
      <title>Residuals</title>
      <show_legend>false</show_legend>
      <tooltip></tooltip>
      <x_axis>
        <title>Raw Values</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title></title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>-10.0</minimum>
          <maximum>10.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Residuals</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalResidR2E</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="xyplot" version="2.0.0">
      <name>X/Y Plot_5</name>
      <x>1030</x>
      <y>600</y>
      <width>470</width>
      <height>340</height>
      <title>Residuals</title>
      <show_legend>false</show_legend>
      <tooltip></tooltip>
      <x_axis>
        <title>EGU Values</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>100.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <visible>true</visible>
      </x_axis>
      <y_axes>
        <y_axis>
          <title></title>
          <autoscale>true</autoscale>
          <log_scale>false</log_scale>
          <minimum>-10.0</minimum>
          <maximum>10.0</maximum>
          <show_grid>false</show_grid>
          <title_font>
            <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
            </font>
          </title_font>
          <scale_font>
            <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
            </font>
          </scale_font>
          <visible>true</visible>
        </y_axis>
      </y_axes>
      <traces>
        <trace>
          <name>Residuals</name>
          <x_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</x_pv>
          <y_pv>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalResidE2R</y_pv>
          <err_pv></err_pv>
          <axis>0</axis>
          <trace_type>1</trace_type>
          <color>
            <color red="0" green="0" blue="255">
            </color>
          </color>
          <line_width>1</line_width>
          <line_style>0</line_style>
          <point_type>0</point_type>
          <point_size>10</point_size>
          <visible>true</visible>
        </trace>
      </traces>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_5</name>
      <actions>
        <action type="open_display">
          <file>full-scaling_pi.bob</file>
          <macros>
            <FIT>E2R</FIT>
            <XAX>EGU</XAX>
            <YAX>Raw</YAX>
          </macros>
          <target>window</target>
          <description>Open Display</description>
        </action>
      </actions>
      <text>...</text>
      <x>470</x>
      <y>880</y>
      <width>40</width>
      <height>20</height>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_71</name>
      <text>Chisq</text>
      <x>1160</x>
      <y>420</y>
      <width>60</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalChisqR2E</pv_name>
      <x>1230</x>
      <y>420</y>
      <width>170</width>
      <height>30</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_72</name>
      <text>Chisq</text>
      <x>1180</x>
      <y>950</y>
      <width>60</width>
      <height>30</height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_3</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalChisqE2R</pv_name>
      <x>1250</x>
      <y>950</y>
      <width>170</width>
      <height>30</height>
    </widget>
    <widget type="led" version="2.0.0">
      <name>LED</name>
      <pv_name>$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEn-RB</pv_name>
      <x>210</x>
      <y>970</y>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_67</name>
      <text>FIle name:</text>
      <x>50</x>
      <y>420</y>
      <height>30</height>
    </widget>
    <widget type="textentry" version="3.0.0">
      <name>Text Entry</name>
      <pv_name>loc://filename("")</pv_name>
      <x>130</x>
      <y>410</y>
      <width>170</width>
      <height>40</height>
    </widget>
    <widget type="fileselector" version="2.0.0">
      <name>File Selector</name>
      <pv_name>loc://filename("")</pv_name>
      <x>320</x>
      <y>410</y>
      <height>40</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_10</name>
      <actions>
        <action type="write_pv">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE=FF)_save_tbl(0)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <text>Save Calibration</text>
      <x>400</x>
      <y>470</y>
      <width>110</width>
      <height>60</height>
      <scripts>
        <script file="EmbeddedPy">
          <text><![CDATA[from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from jarray import array
import csv

run = PVUtil.getInt(pvs[0])
if run == 1:
    file_name = PVUtil.getString(pvs[1])
    col1 = PVUtil.getDoubleArray(pvs[2])
    col2 = PVUtil.getDoubleArray(pvs[3])
    with open(file_name, "w") as csvfile:
        writer = csv.writer(csvfile)
        for i in range(len(col1)):
            writer.writerow([col1[i], col2[i]])

    pvs[0].setValue(0)]]></text>
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE=FF)_save_tbl(0)</pv_name>
          <pv_name trigger="false">loc://filename("")</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</pv_name>
        </script>
      </scripts>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_3</name>
      <actions>
        <action type="write_pv">
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE=FF)_load_tbl(0)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <text>Load calibration tables</text>
      <x>400</x>
      <y>410</y>
      <width>110</width>
      <height>50</height>
      <scripts>
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from jarray import array
import csv

run = PVUtil.getInt(pvs[0])
if run == 1:
    file_name = PVUtil.getString(pvs[1])

    def decomment(csvfile):
        for row in csvfile:
            raw = row.split('#')[0].strip()
            if raw: yield raw
    with open(file_name) as csvfile:
        reader = csv.reader(decomment(csvfile))
        rows1 = []
        rows2 = []
        for row in reader:
            rows1.append(float(row[0]))
            rows2.append(float(row[1]))

    pvs[2].setValue(array(rows1, 'd'))
    pvs[3].setValue(array(rows2, 'd'))
    pvs[0].setValue(0)]]></text>
          <pv_name>loc://$(PD=LLRF:)$(RD=DIG01:)$(PI_TYPE=FF)_load_tbl(0)</pv_name>
          <pv_name trigger="false">loc://filename("")</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalEGU</pv_name>
          <pv_name trigger="false">$(PD=LLRF:)$(RD=DIG01:)RFCtrl$(PI_TYPE=FF)-CalRaw</pv_name>
        </script>
      </scripts>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
</display>
