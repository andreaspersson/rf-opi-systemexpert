# RF OPIs - structure README

The top-rf OPI contains tabs, of which each contain a tab representing an embedded display. Each embedded display is a shell OPI consisting of macros, including one pointing to the OPI to display. Hence, generic OPIs can be constructed and populated by by the macros shell OPIs.

In this case, the generic OPI is a top-level main menu of RF OPIs. This is displayed in the flowchart image beneath.

![rf top-level structure png](structure.png)

The main benefit is that the macro shells can be reused from any other OPI, in any other deployed repository, provided that this repository is deployed to the target system. Hence, the macros can be defined in a centralized single location and then point to the OPI that the macros shall populate. Thus, all OPIs that require the specific PVs can use the same macros shell OPI (one per system). See figure beneath for the comparison when 5 OPIs show data from the same system and subsystem(s), hence sharing the same macros. Should a change arise, without the macros shell OPI, the developer has to change each OPI, whilst with the macros shell OPI, the developer only has to change within the macros shell OPI.

![structure comparison png](structure_comp.png)
